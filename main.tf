provider "aws" {
  # Configuration options
  region     = var.region_instance
  access_key = var.access_key_instance
  secret_key = var.secret_key_instance
  token= var.token_key_instance
}

module "demo_instance" {
    source = "./modules/ec2_ready"
#    public_ssh_key = var.public_ssh_key
#    aws_instance_name =
    key_name = var.key_name
    count_instances = var.count_instances
    vpc_name = var.vpc_name
    aws_instance_name = var.aws_instance_name
    public_ssh_key = var.public_ssh_key
    ec2_ami = var.ec2_ami
    instance_type = var.instance_type
}