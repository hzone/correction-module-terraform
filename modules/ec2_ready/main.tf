#Creation d'une instance
#resource "aws_instance" ${var.aws_instance_name} {
resource "aws_instance" "ec2-abd" {
  count = "${var.count_instances}"
  ami = "${var.ec2_ami}"
  instance_type = "${var.instance_type}"
  tags = {
    Name = "ec2-abd-test"#${count.index}" 
    }
}
resource "aws_key_pair" "deployer" {
  key_name   = "${var.key_name}"
  public_key = "${var.public_ssh_key}"
}
#Creatio du VPC ABD
resource "aws_vpc" "vpc_abd" {
  cidr_block       = "10.10.0.0/16"
  instance_tenancy = "default"

  tags= {
    Name= "${var.vpc_name}"
  }
}

#creation reseau
resource "aws_subnet" "abd_subnet" {
  vpc_id            = aws_vpc.vpc_abd.id
  cidr_block        = "10.10.1.0/24"
  availability_zone = "us-east-1a" # zone disponible a

  tags= {
    Name= "subnet_abd"
  }
}

#creation gateway
resource "aws_internet_gateway" "gw_abd" {
    vpc_id = aws_vpc.vpc_abd.id

  tags= {
    Name= "gateway_abd"
  }
}

#les routes
resource "aws_route_table" "publique_route_abd" {
    vpc_id = aws_vpc.vpc_abd.id
    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = aws_internet_gateway.gw_abd.id
    }
    
}

resource "aws_route_table_association" "a" {
    subnet_id      = aws_subnet.abd_subnet.id
    route_table_id = aws_route_table.publique_route_abd.id
}


