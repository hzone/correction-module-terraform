variable "public_ssh_key" {
    description = "public ssh key for ec2 instance"
    type = string
  
}
variable "ec2_ami" {
    description = "ID of AMI for ec2 instance"
    type = string
}

variable "key_name" {
    description = "Name of public ssh key for ec2 instance"
    type = string
}
variable "instance_type" {
    description = "type of instance ec2"
    type = string
}
variable "count_instances" {
    description = "number of instance to create"
    type = number
  
}
variable "vpc_name" {
    description = "name of vpc"
    type = string
}
variable "aws_instance_name" {
    description = "name of aws instance"
    type = string
}