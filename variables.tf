variable "region_instance" {
  description = "aws region "
  type = string
  default = "us-east-1"
}
variable "access_key_instance" {
    description = "access key "
    type = string
}
variable "secret_key_instance" {
    description = "secret key "
    type = string
}
variable "token_key_instance" {
    description = "token key "
    type = string
}

variable "key_name" {}
variable "count_instances" {}
variable "vpc_name" {}
variable "aws_instance_name" {}
variable "public_ssh_key" {}
variable "ec2_ami" {}
variable "instance_type" {}
